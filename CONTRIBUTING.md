# Contribute To dpkg-clean

Community made patches, bug reports and contributions are always welcome!

When contributing please ensure you follow the guidelines below so that we can keep on top of things.

## Getting Started

* Submit a ticket for your issue, assuming one does not already exist.
  * Raise it on our [Issue Tracker](https://gitlab.com/pop-planet/dpkg-clean/issues)
  * Clearly describe the issue including steps to reproduce the bug.

## Making Changes

* Fork the repository on GitLab
* Make the changes to your forked repository
  * Ensure you stick to the [black](https://github.com/psf/black) standards
  * Please ensure that if you add or edit a method, you update the documentation as necessary
* When committing, reference your issue (if present) and include a note about the fix
* If possible, and if applicable, please also add/update unit tests for your changes
* Push the changes to your fork and submit a pull request to the 'master' branch of the repository
