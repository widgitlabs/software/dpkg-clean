# Changelog

## 2.0.0 (2020-02-16)

* Improved: Rewritten from the ground up as a python app

## 1.0.1 (2019-01-09)

* Improved: Converted original function to a standalone script for release

## 1.0.0 (pre-2019)

* First official release!
