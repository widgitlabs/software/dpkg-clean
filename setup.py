#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from distutils.core import setup

setup(
    name='dpkg-clean',
    version='2.0.0',
    description='Simple cleanup tool for leftover dpkg configs',
    url='https://gitlab.com/pop-planet/dpkg-clean',
    author='Pop!_Planet Team',
    author_email='support@pop-planet.info',
    license='GPL3',
    scripts=['bin/dpkg-clean'],
    data_files=[("share/man/man8", ["docs/dpkg-clean.8"]),]
)