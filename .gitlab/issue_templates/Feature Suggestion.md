# Feature Suggestion

## Problem to solve

(What problem do we want to solve?)

## Further details

(Include use cases, benefits, and/or goals)

## Proposal

(How are we going to solve the problem? Try to include the user journey!)

## Links / references

(Include any relevant links or references for associated or similar products)

/label ~"Feature Suggestion"
